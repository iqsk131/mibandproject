package com.shumiq.mibandproject.devices.liveview;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.shumiq.mibandproject.GBException;
import com.shumiq.mibandproject.R;
import com.shumiq.mibandproject.devices.AbstractDeviceCoordinator;
import com.shumiq.mibandproject.devices.InstallHandler;
import com.shumiq.mibandproject.devices.SampleProvider;
import com.shumiq.mibandproject.entities.DaoSession;
import com.shumiq.mibandproject.entities.Device;
import com.shumiq.mibandproject.impl.GBDevice;
import com.shumiq.mibandproject.impl.GBDeviceCandidate;
import com.shumiq.mibandproject.model.ActivitySample;
import com.shumiq.mibandproject.model.DeviceType;

public class LiveviewCoordinator extends AbstractDeviceCoordinator {
    @Override
    public DeviceType getSupportedType(GBDeviceCandidate candidate) {
        String name = candidate.getDevice().getName();
        if (name != null && name.startsWith("LiveView")) {
            return DeviceType.LIVEVIEW;
        }
        return DeviceType.UNKNOWN;
    }

    @Override
    public DeviceType getDeviceType() {
        return DeviceType.LIVEVIEW;
    }

    @Override
    public Class<? extends Activity> getPairingActivity() {
        return null;
    }

    @Override
    public Class<? extends Activity> getPrimaryActivity() {
        return null;
    }

    @Override
    public InstallHandler findInstallHandler(Uri uri, Context context) {
        return null;
    }

    @Override
    public boolean supportsActivityDataFetching() {
        return false;
    }

    @Override
    public boolean supportsActivityTracking() {
        return false;
    }

    @Override
    public SampleProvider<? extends ActivitySample> getSampleProvider(GBDevice device, DaoSession session) {
        return null;
    }

    @Override
    public boolean supportsScreenshots() {
        return false;
    }

    @Override
    public boolean supportsAlarmConfiguration() {
        return false;
    }

    @Override
    public boolean supportsHeartRateMeasurement(GBDevice device) {
        return false;
    }

    @Override
    public int getTapString() {
        //TODO: changeme
        return R.string.tap_connected_device_for_activity;
    }

    @Override
    public String getManufacturer() {
        return "Sony Ericsson";
    }

    @Override
    public boolean supportsAppsManagement() {
        return false;
    }

    @Override
    public Class<? extends Activity> getAppsManagementActivity() {
        return null;
    }

    @Override
    protected void deleteDevice(@NonNull GBDevice gbDevice, @NonNull Device device, @NonNull DaoSession session) throws GBException {
        // nothing to delete, yet
    }
}
