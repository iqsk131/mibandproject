package com.shumiq.mibandproject.devices.miband;

public enum DateTimeDisplay {
    TIME,
    DATE_TIME
}
