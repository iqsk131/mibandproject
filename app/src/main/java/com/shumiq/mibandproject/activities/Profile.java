package com.shumiq.mibandproject.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.api.model.StringList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shumiq.mibandproject.R;
import com.shumiq.mibandproject.service.devices.miband.MiBandSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by IQ on 20/2/2560.
 */

public class Profile extends GBActivity {

    private EditText etName;
    private Spinner spGender;
    private LinearLayout loBirthdate;
    private TextView tvBirthdate;
    private Button btSave;
    //private Button btLogout;
    private static final Logger LOG = LoggerFactory.getLogger(Profile.class);
    private int year=1900;
    private int month=0;
    private int day =1;
    private boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        etName = (EditText)findViewById(R.id.etName);
        btSave = (Button) findViewById(R.id.btSaveProfile);
        //btLogout = (Button)findViewById(R.id.btLogout);

        spGender = (Spinner)findViewById(R.id.spGender);
        final ArrayAdapter<CharSequence> genderList = ArrayAdapter.createFromResource(this, R.array.genderList, android.R.layout.simple_spinner_item);
        genderList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(genderList);

        tvBirthdate = (TextView) findViewById(R.id.tvBirthDateValue);
        loBirthdate = (LinearLayout) findViewById(R.id.loBirthDate);
        loBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(Profile.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                        year = y;
                        month = m;
                        day = d;
                        tvBirthdate.setText(dateToString());
                    }
                },year,month,day).show();

            }
        });

        //spBirthdate.setPrompt("Test");

        FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(isUpdate)return;
                isUpdate=true;
                try {
                    String name = "";
                    String gender = "M";
                    if (dataSnapshot.child("name").getValue(String.class) == null) {
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/name").setValue("");
                    } else {
                        name = dataSnapshot.child("name").getValue(String.class);
                    }
                    if (dataSnapshot.child("gender").getValue(String.class) == null) {
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/gender").setValue("M");
                    } else {
                        gender = dataSnapshot.child("gender").getValue(String.class);
                    }
                    if (dataSnapshot.child("year").getValue() == null) {
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/year").setValue("1900");
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/month").setValue("0");
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/day").setValue("1");
                    } else {
                        year = Integer.parseInt("" + dataSnapshot.child("year").getValue());
                        month = Integer.parseInt("" + dataSnapshot.child("month").getValue());
                        day = Integer.parseInt("" + dataSnapshot.child("day").getValue());
                    }

                    etName.setText(name);
                    etName.setSelection(name.length());
                    if (gender.equals("M")) {
                        spGender.setSelection(0);
                    } else {
                        spGender.setSelection(1);
                    }
                    tvBirthdate.setText(dateToString());
                } catch (Exception e){ LOG.info(e.toString()); }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/name").setValue("" + etName.getText());
                    if (spGender.getSelectedItemId()==0) {
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/gender").setValue("M");
                    } else {
                        FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/gender").setValue("F");
                    }
                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/year").setValue(year);
                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/month").setValue(month);
                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/day").setValue(day);
                    Toast.makeText(Profile.this, "บันทึกแล้ว!", Toast.LENGTH_SHORT).show();
                } catch (Exception e){
                    Toast.makeText(Profile.this, "ไม่สามารถบันทึกได้..", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /*btLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AuthUI.getInstance()
                    .signOut(Profile.this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(Profile.this, DashBoard.class));
                            finish();
                        }
                    });
            }
        });*/
    }

    private String dateToString(){
        String monthString;
        switch (month){
            case 1 :
                monthString = "กุมภาพันธ์";
                break;
            case 2 :
                monthString = "มีนาคม";
                break;
            case 3 :
                monthString = "เมษายน";
                break;
            case 4 :
                monthString = "พฤษภาคม";
                break;
            case 5 :
                monthString = "มิถุนายน";
                break;
            case 6 :
                monthString = "กรกฎาคม";
                break;
            case 7 :
                monthString = "สิงหาคม";
                break;
            case 8 :
                monthString = "กันยายน";
                break;
            case 9 :
                monthString = "ตุลาคม";
                break;
            case 10 :
                monthString = "พฤศจิกายน";
                break;
            case 11 :
                monthString = "ธันวาคม";
                break;
            default :
                monthString = "มกราคม";
                break;
        }
        return day + " " + monthString + " " + (year+543);
    }
}
