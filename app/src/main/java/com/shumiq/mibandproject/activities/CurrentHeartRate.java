package com.shumiq.mibandproject.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shumiq.mibandproject.R;
import com.shumiq.mibandproject.service.devices.miband.MiBandSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;

public class CurrentHeartRate extends GBActivity {

    private TextView tvHeartRate;
    private TextView tvActivity;
    private RelativeLayout rlActivity;
    private ImageView ivActivity;
    private static final Logger LOG = LoggerFactory.getLogger(CurrentHeartRate.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_heartrate);

        tvHeartRate = (TextView) findViewById(R.id.tvHeartRate);

        tvActivity = (TextView) findViewById(R.id.tvCurrentActivity);
        rlActivity = (RelativeLayout) findViewById(R.id.rlCurrentActivity);
        ivActivity = (ImageView) findViewById(R.id.ivCurrentActivity);

        FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    String currentDate = String.format("%02d-%02d", Calendar.getInstance().get(Calendar.DATE), Calendar.getInstance().get(Calendar.MONTH) + 1);
                    //String currentTime = String.format("%02d:%02d", Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE));
                    if(dataSnapshot.child("hr/" + currentDate).getValue()==null)throw new Exception();
                    Iterable<DataSnapshot> hrValues = dataSnapshot.child("hr/" + currentDate).getChildren();
                    ArrayList<DataSnapshot> hrValuesList = Lists.newArrayList(hrValues);
                    String latestHR = "";
                    if (hrValuesList.size() > 0)
                        latestHR += "" + hrValuesList.get(hrValuesList.size() - 1).getValue();
                    if (DashBoard.isConnected) {
                        tvHeartRate.setText(latestHR);
                    }
                }
                catch (Exception e){
                    try {
                        tvHeartRate.setText(DashBoard.latestHR);
                    } catch (Exception e2){
                        tvHeartRate.setText("0");
                    }
                }

                int hr;
                try{
                    hr = Integer.parseInt("" + tvHeartRate.getText());
                } catch (Exception e) {hr=0;}
                if(hr<(220-DashBoard.userAge)*0.5){
                    tvActivity.setText("\n\n\nไม่มีการออกกำลัง");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_1);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_1));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_1));
                }
                else if(hr<(220-DashBoard.userAge)*0.6){
                    tvActivity.setText("\n\n\nออกกำลังน้อยมาก");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_2);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_2));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_2));
                }
                else if(hr<(220-DashBoard.userAge)*0.7){
                    tvActivity.setText("\n\n\nออกกำลังน้อย");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_3);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_3));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_3));
                }
                else if(hr<(220-DashBoard.userAge)*0.8){
                    tvActivity.setText("\n\n\nออกกำลังปานกลาง");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_4);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_4));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_4));
                }
                else if(hr<(220-DashBoard.userAge)*0.9){
                    tvActivity.setText("\n\n\nออกกำลังมาก");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_5);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_5));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_5));
                }
                else {
                    tvActivity.setText("\n\n\nออกกำลังมากที่สุด");
                    rlActivity.setBackgroundResource(R.color.hr_history_bg_6);
                    ivActivity.setColorFilter(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_6));
                    tvActivity.setTextColor(ContextCompat.getColor(CurrentHeartRate.this,R.color.hr_history_text_6));
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });
    }
}