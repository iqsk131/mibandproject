package com.shumiq.mibandproject.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shumiq.mibandproject.R;
import com.shumiq.mibandproject.service.devices.miband.MiBandSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by IQ on 20/2/2560.
 */

public class DayStat extends GBActivity {

    private LineChart chart;
    private RecyclerView rvHistory;
    private static final Logger LOG = LoggerFactory.getLogger(DayStat.class);
    private int year=70;
    private boolean firstTime=true;
    private HistoryAdapter hAdapter;
    private LinearLayoutManager llmHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_stat);

        firstTime=true;
        rvHistory = (RecyclerView) findViewById(R.id.rvHistory);
        rvHistory.setHasFixedSize(true);
        llmHistory = new LinearLayoutManager(this);
        rvHistory.setLayoutManager(llmHistory);
        hAdapter = new HistoryAdapter(new ArrayList<HrHistory>());
        rvHistory.setAdapter(hAdapter);
        //hAdapter.notifyDataSetChanged();

        chart=(LineChart) findViewById(R.id.chDayStat);


        FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    String currentDate = String.format("%02d-%02d", Calendar.getInstance().get(Calendar.DATE), Calendar.getInstance().get(Calendar.MONTH) + 1);
                    DataSnapshot hrDatasnapshot = dataSnapshot.child("hr/" + currentDate);
                    List<Entry> entries = new ArrayList<Entry>();
                    List<HrHistory> histories = new ArrayList<HrHistory>();
                    float maxTimeValue=0f;

                    /////// history
                    int typeBefore=-1;
                    int hrSum=0;
                    int hrCount=0;
                    boolean isAlert=false;
                    String startTime = "";
                    String endTime = "";
                    ///////

                    for (DataSnapshot hrValue : hrDatasnapshot.getChildren()) {
                        if(hrValue.getValue(Integer.class)==0){
                            continue;
                        }
                        String time = hrValue.getKey();
                        int timeValue = Integer.parseInt(time.substring(0, 2)) * 60 + Integer.parseInt(time.substring(3, 5));
                        maxTimeValue=timeValue;
                        entries.add(new Entry(timeValue, Float.parseFloat("" + hrValue.getValue())));

                        /////// history
                        int currentType = DashBoard.hrZone(hrValue.getValue(Integer.class));
                        if(currentType == typeBefore){
                            hrSum += hrValue.getValue(Integer.class);
                            hrCount++;
                            endTime=time;
                            if(dataSnapshot.child("alert/"+currentDate+"/"+time).getValue()!=null){
                                isAlert=true;
                            }
                        }
                        else {
                            if(typeBefore!=-1){
                                int avgHr = hrSum/hrCount;
                                if(hrCount>1){
                                    histories.add(0, new HrHistory("", "เวลา " + startTime + " - " + endTime, "" + avgHr + "<font color=red>♥</font>",isAlert));
                                }
                                else{
                                    histories.add(0, new HrHistory("", "เวลา " + startTime, "" + avgHr + "<font color=red>♥</font>",isAlert));
                                }
                            }
                            typeBefore=currentType;
                            hrSum = hrValue.getValue(Integer.class);
                            hrCount=1;
                            startTime=time;
                            if(dataSnapshot.child("alert/"+currentDate+"/"+time).getValue()!=null){
                                isAlert=true;
                            }
                            else{
                                isAlert=false;
                            }
                        }
                        //////
                    }
                    int avgHr = hrSum/hrCount;
                    if(hrCount>1){
                        histories.add(0, new HrHistory("", "เวลา " + startTime + " - " + endTime, "" + avgHr + "<font color=red>♥</font>",isAlert));
                    }
                    else{
                        histories.add(0, new HrHistory("", "เวลา " + startTime, "" + avgHr + "<font color=red>♥</font>",isAlert));
                    }
                    if(histories.size()>0){
                        hAdapter.histories = histories;
                        if(llmHistory.findFirstVisibleItemPosition()==0){
                            rvHistory.setAdapter(hAdapter);
                        }
                    }
                    if (entries.size() > 0) {
                        LineDataSet dataSet = new LineDataSet(entries, "Heart Rate");
                        dataSet.setColor(ColorTemplate.MATERIAL_COLORS[2]);
                        dataSet.setFillColor(ColorTemplate.MATERIAL_COLORS[2]);
                        dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
                        dataSet.setDrawFilled(true);
                        dataSet.setDrawCircles(false);
                        LineData lineData = new LineData(dataSet);
                        chart.setData(lineData);
                        Description d = new Description();
                        d.setText("");
                        chart.setDescription(d);
                        chart.getXAxis().setValueFormatter(new TimeFormatter());
                        if(firstTime) {
                            chart.moveViewToX(maxTimeValue);
                            chart.setVisibleXRangeMinimum(60f);
                            chart.zoomToCenter(maxTimeValue, 0);
                            firstTime=false;
                        }
                        chart.setVisibleXRangeMinimum(10f);
                        chart.setMaxVisibleValueCount(10);
                        chart.invalidate(); // refresh
                    }
                } catch (Exception e) { }
            }

            @Override
            public void onCancelled(DatabaseError error) {

            }
        });

    }

    public class TimeFormatter implements IAxisValueFormatter {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            int hour = (int)(value / 60);
            int minute = (int)value %60;
            return String.format("%02d:%02d",hour,minute);
        }
    }

}

class HrHistory {

    private String activity;
    private String time;
    private String hr;
    private boolean isAlert;

    public HrHistory() {
    }

    public HrHistory(String activity, String time, String hr, boolean isAlert) {
        this.activity = activity;
        this.time = time;
        this.hr = hr;
        this.isAlert=isAlert;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public boolean isAlert() {
        return isAlert;
    }

    public void setAlert(boolean alert) {
        isAlert = alert;
    }
}

class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {
    List<HrHistory> histories;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class HistoryViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView cvActivity;
        public TextView tvActivityState;
        public TextView tvActivityTime;
        public TextView tvActivityHR;
        public ImageView ivAlert;
        public HistoryViewHolder(View v) {
            super(v);
            cvActivity = (CardView) v.findViewById(R.id.cvHistory);
            tvActivityState = (TextView) v.findViewById(R.id.tvActivityState);
            tvActivityTime = (TextView) v.findViewById(R.id.tvActivityTime);
            tvActivityHR = (TextView) v.findViewById(R.id.tvActivityHR);
            ivAlert = (ImageView) v.findViewById(R.id.ivAlert);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HistoryAdapter(List<HrHistory> histories) {
        this.histories = histories;
    }

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_hr_history, viewGroup, false);
        HistoryViewHolder pvh = new HistoryViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder historyViewHolder, int i) {
        String hrString ="" + Html.fromHtml(histories.get(i).getHr());
        int hr = Integer.parseInt(hrString.substring(0,hrString.length()-1));
        if(hr<(220-DashBoard.userAge)*0.5){
            historyViewHolder.tvActivityState.setText("ไม่มีการออกกำลัง");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_1);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_1));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_1));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_1));
        }
        else if(hr<(220-DashBoard.userAge)*0.6){
            historyViewHolder.tvActivityState.setText("ออกกำลังน้อยมาก");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_2);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_2));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_2));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_2));
        }
        else if(hr<(220-DashBoard.userAge)*0.7){
            historyViewHolder.tvActivityState.setText("ออกกำลังน้อย");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_3);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_3));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_3));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_3));
        }
        else if(hr<(220-DashBoard.userAge)*0.8){
            historyViewHolder.tvActivityState.setText("ออกกำลังปานกลาง");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_4);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_4));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_4));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_4));
        }
        else if(hr<(220-DashBoard.userAge)*0.9){
            historyViewHolder.tvActivityState.setText("ออกกำลังมาก");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_5);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_5));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_5));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_5));
        }
        else {
            historyViewHolder.tvActivityState.setText("ออกกำลังมากที่สุด");
            historyViewHolder.cvActivity.setBackgroundResource(R.color.hr_history_bg_6);
            historyViewHolder.tvActivityState.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_6));
            historyViewHolder.tvActivityTime.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_6));
            historyViewHolder.tvActivityHR.setTextColor(ContextCompat.getColor(historyViewHolder.cvActivity.getContext(),R.color.hr_history_text_6));
        }

        //historyViewHolder.tvActivityState.setText(histories.get(i).getActivity());
        historyViewHolder.tvActivityTime.setText(histories.get(i).getTime());
        historyViewHolder.tvActivityHR.setText(Html.fromHtml(histories.get(i).getHr()));

        if(histories.get(i).isAlert()){
            historyViewHolder.ivAlert.setVisibility(View.VISIBLE);
        }
        else{
            historyViewHolder.ivAlert.setVisibility(View.GONE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return histories.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}