package com.shumiq.mibandproject.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.common.collect.Lists;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.shumiq.mibandproject.GBApplication;
import com.shumiq.mibandproject.service.devices.miband.MiBandSupport;

import com.shumiq.mibandproject.R;
import com.shumiq.mibandproject.util.GB;
import com.shumiq.mibandproject.util.Prefs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Text;

import java.util.Arrays;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DashBoard extends GBActivity implements View.OnClickListener {

    private ImageButton btSetting;
    private ImageButton btCurrentHR;
    private TextView tvCurrentHR;
    private ImageButton btProfile;
    private ImageButton btDayStat;
    private ImageButton btChat;
    private ImageButton btTutorial;
    private Thread checkConnectionThread;
    private static final Logger LOG = LoggerFactory.getLogger(DashBoard.class);
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final int RC_SIGN_IN = 1;
    public static String userUID = "";
    public static boolean playAlarm=false;
    public static FirebaseUser user=null;
    public static String userName="";
    public static int userAge=70;
    public static boolean isConnected=false; //Added by IQ
    public static int latestHR=0; //Added by IQ

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true
        getSupportActionBar().setLogo(R.drawable .ic_launcher_b);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAuth = FirebaseAuth.getInstance();
        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if(user != null){
                    userUID=user.getUid();
                    LOG.info("IQ: UID - " + userUID);
                    FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID+"/email").setValue(user.getEmail());
                    initComponent();
                }
                else{
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setProviders(Arrays.asList(new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
                                            new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()))
                                    .setTheme(R.style.AuthTheme)
                                    .setLogo(R.drawable.logo_b)
                                    .build(),
                            RC_SIGN_IN);
                }
            }
        };
    }

    private void initComponent(){
        btSetting = (ImageButton) findViewById(R.id.btSetting);
        btSetting.setOnClickListener(this);

        btCurrentHR = (ImageButton) findViewById(R.id.btCurrentHR);
        btCurrentHR.setOnClickListener(this);
        tvCurrentHR = (TextView) findViewById(R.id.btCurrentHRLabel);

        btProfile = (ImageButton) findViewById(R.id.btProfile);
        btProfile.setOnClickListener(this);

        btDayStat = (ImageButton) findViewById(R.id.btDayStat);
        btDayStat.setOnClickListener(this);

        btChat = (ImageButton) findViewById(R.id.btChat);
        btChat.setOnClickListener(this);

        btTutorial = (ImageButton) findViewById(R.id.btTutorial);
        btTutorial.setOnClickListener(this);

        Runnable checkConnection = new Runnable() {
            public void run() {
                while(true) {
                    try {
                        if(DashBoard.isConnected){
                            btSetting.setImageResource(R.drawable.btn_setting);
                            btCurrentHR.setImageResource(R.drawable.btn_hr);
                        }
                        else{
                            btSetting.setImageResource(R.drawable.btn_setting_con);
                            btCurrentHR.setImageResource(R.drawable.btn_hr_con);
                            tvCurrentHR.setText("");
                        }
                        Thread.sleep(1000);
                    } catch (Exception e){

                    }
                }
            }
        };
        if(checkConnectionThread!=null && checkConnectionThread.isAlive())checkConnectionThread.interrupt();
        checkConnectionThread = new Thread(checkConnection);
        checkConnectionThread.start();


        FirebaseDatabase.getInstance().getReference("user/"+DashBoard.userUID).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    try {
                        userName = dataSnapshot.child("name").getValue(String.class);
                        userAge = Math.min(70, Calendar.getInstance().get(Calendar.YEAR) - dataSnapshot.child("year").getValue(Integer.class));
                    }
                    catch (Exception e){
                        userName="";
                        userAge=70;
                    }
                    String currentDate = String.format("%02d-%02d", Calendar.getInstance().get(Calendar.DATE), Calendar.getInstance().get(Calendar.MONTH) + 1);
                    for (DataSnapshot dailyHrValues : dataSnapshot.child("hr").getChildren()) {
                        //LOG.info("IQ: " + dailyHrValues.getKey() + " " + currentDate);
                        if (!dailyHrValues.getKey().equals(currentDate)) {
                            FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/hr/" + dailyHrValues.getKey()).setValue(null);
                            try {
                                FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/alert/" + dailyHrValues.getKey()).setValue(null);
                            } catch (Exception e){}
                            continue;
                        }
                        Iterable<DataSnapshot> hrValues = dataSnapshot.child("hr/" + currentDate).getChildren();
                        ArrayList<DataSnapshot> hrValuesList = Lists.newArrayList(hrValues);
                        String latestHR = "";
                        String beforeLatestHR = "";
                        if (hrValuesList.size() > 0)
                            latestHR += "" + hrValuesList.get(hrValuesList.size() - 1).getValue();
                        if (hrValuesList.size() > 1)
                            beforeLatestHR += "" + hrValuesList.get(hrValuesList.size() - 2).getValue();
                        /*for (DataSnapshot hrValue : hrValues) {
                            LOG.info("IQ: " + hrValue.getRef().toString());
                            beforeLatestHR = latestHR;
                            latestHR = "" + hrValue.getValue(Long.class);
                        }*/
                        //String dashboardHRText = "Current HeartRate:\n" + latestHR;
                        if (DashBoard.isConnected) {

                            if (hrValuesList.size()>1 && !DashBoard.playAlarm) {
                                boolean isHigh=true;
                                int lastTime = Integer.parseInt(hrValuesList.get(hrValuesList.size()-1).getKey().substring(0, 2)) * 60 + Integer.parseInt(hrValuesList.get(hrValuesList.size()-1).getKey().substring(3, 5));
                                for(int i=hrValuesList.size()-1;i>=0;i--){
                                    if(lastTime-5 >= Integer.parseInt(hrValuesList.get(i).getKey().substring(0, 2)) * 60 + Integer.parseInt(hrValuesList.get(i).getKey().substring(3, 5))){
                                        break;
                                    }
                                    if(hrZone(hrValuesList.get(i).getValue(Integer.class))<=4){
                                        isHigh = false;
                                        break;
                                    }
                                }
                                if(isHigh) {
                                    LOG.info("IQ: High HR Alert!!");
                                    FirebaseDatabase.getInstance().getReference("user/" + DashBoard.userUID + "/alert/" + currentDate + "/" + hrValuesList.get(hrValuesList.size() - 1).getKey()).setValue("true");
                                    DashBoard.playAlarm = true;
                                    MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alarm);
                                    mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                    mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            DashBoard.playAlarm = false;
                                        }
                                    });
                                    mPlayer.start();
                                }
                            }
                            tvCurrentHR.setText(latestHR);
                            GB.updateNotification("อัตราการเต้นของหัวใจขณะนี้คือ " + latestHR + " ครั้ง/นาที",true,DashBoard.this);
                        }
                    }
                } catch (Exception e){
                    LOG.info("IQ: " + e.toString());
                    try {
                        tvCurrentHR.setText(DashBoard.latestHR);
                    } catch (Exception e2){
                        tvCurrentHR.setText("0");
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {

            }

        });

        //Control Center
        Prefs prefs = GBApplication.getPrefs();
        if (prefs.getBoolean("firstrun", true)) {
            prefs.getPreferences().edit().putBoolean("firstrun", false).apply();
            Intent enableIntent = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
            startActivity(enableIntent);
        }

    }

    @Override
    public void onClick(View v){
        if(v == btSetting){
            Intent intent = new Intent(this, ControlCenter.class);
            startActivity(intent);
        }
        if(v == btCurrentHR){
            if(DashBoard.isConnected) {
                Intent intent = new Intent(this, CurrentHeartRate.class);
                startActivity(intent);
            }
            else{
                Intent intent = new Intent(this, ControlCenter.class);
                startActivity(intent);
            }
        }

        if(v == btProfile){
            Intent intent = new Intent(this, Profile.class);
            startActivity(intent);
        }

        if(v == btDayStat){
            Intent intent = new Intent(this, DayStat.class);
            startActivity(intent);
        }

        if(v == btChat){
            Intent intent = new Intent(this, ChatMain.class);
            startActivity(intent);
        }

        if(v == btTutorial){
            Intent intent = new Intent(this, Tutorial.class);
            startActivity(intent);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAuthStateListener != null) {
            mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                startActivity(new Intent(DashBoard.this, DashBoard.class));
                                finish();
                            }
                        });
                return true;
            /*case R.id.action_quit:
                GBApplication.quit();
                return true;*/
        }

        return super.onOptionsItemSelected(item);
    }

    public static int hrZone(int hr) {
        if (hr < (220 - DashBoard.userAge) * 0.5) {
            return 0;
        } else if (hr < (220 - DashBoard.userAge) * 0.6) {
            return 1;
        } else if (hr < (220 - DashBoard.userAge) * 0.7) {
            return 2;
        } else if (hr < (220 - DashBoard.userAge) * 0.8) {
            return 3;
        } else if (hr < (220 - DashBoard.userAge) * 0.9) {
            return 4;
        } else {
            return 5;
        }
    }
}