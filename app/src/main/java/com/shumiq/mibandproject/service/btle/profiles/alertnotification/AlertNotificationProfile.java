package com.shumiq.mibandproject.service.btle.profiles.alertnotification;

import com.shumiq.mibandproject.service.btle.AbstractBTLEDeviceSupport;
import com.shumiq.mibandproject.service.btle.profiles.AbstractBleProfile;

public class AlertNotificationProfile<T extends AbstractBTLEDeviceSupport> extends AbstractBleProfile<T> {
    public AlertNotificationProfile(T support) {
        super(support);
    }


}
